# bc-tx-project

## Notes
- Challenge under `/app` folder, uses `create-react-app`.
- Includes `.nvmrc` with the node setup used
- Used `yarn` as the package manager
- Uses the latest version of Tailwind CSS for styling, and PostCSS 7 is not compatible at the moment with `create-react-app`, so `craco` is used in order to override `webpack` configs

## Instructions
- Run `yarn start` on root folder to spawn server and app concurrently