export const API_URL = 'http://localhost:8888';
export const BTC_TXS_URL = `/btc-txs`;
export const ETH_TXS_URL = `/eth-txs`;
export const CUSTODIAL_TXS_URL = `/custodial-txs`;
export const PRICES_URL = `/prices`;
export const POLL_INTERVAL = 30 * 1000;