import { renderHook, act } from '@testing-library/react-hooks';
import { format, formatDistance } from 'date-fns';

import { txs as txsData } from 'api/mocks/ethereumTxs';
import { EthereumTx } from 'types/Ethereum';
import { fromWeis, getPrice } from 'utils';

import { useSearch } from './useSearch';

const txs: EthereumTx[] = txsData.map(tx => ({
  ...tx,
  amountCurrency: getPrice(fromWeis(tx.amount), 0),
  amountCoin: fromWeis(tx.amount),
  date: tx.insertedAt * 1000
}));

describe('useSearch hook', () => {
  it('should return item according to search criteria', () => {
    const { result } = renderHook(() => useSearch(txs));
      
    act(() => {
      result.current.setFilter(txs[0].amount.toString());
    });
      
    expect(result.current.collection).toEqual([txs[0]]);
  });

  it('should return item according to date', () => {
    const { result } = renderHook(() => useSearch(txs));
      
    act(() => {
      result.current.setFilter(format(new Date(txs[0].date), 'dd/MM/yyyy HH:mm:ss'));
    });
    expect(result.current.collection).toEqual([txs[0]]);

    act(() => {
      result.current.setFilter(formatDistance(
        new Date(txs[0].date), 
        new Date()
      ));
    });
    expect(result.current.collection).toEqual([txs[0]]);
  });

  it('should return an empty collection when on item is found', () => {
    const { result } = renderHook(() => useSearch(txs));
      
    act(() => {
      result.current.setFilter('$$$$$$$$$$$');
    });
      
    expect(result.current.collection).toEqual([]);
  });
});
