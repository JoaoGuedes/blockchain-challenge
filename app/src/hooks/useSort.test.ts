import { renderHook, act } from '@testing-library/react-hooks';

import { txs } from 'api/mocks/ethereumTxs';

import { useSort } from './useSort';

describe('useSort hook', () => {
  it('should sort items in collection by increasing amount', () => {
    const { result } = renderHook(() => useSort(txs));
      
    act(() => {
      result.current.setField('amount');
      result.current.setDirection(1);
    });
      
    result.current.collection.forEach((value, index, array) => {
      if (index < array.length-2) {
        expect(value.amount).toBeLessThanOrEqual(array[index+1].amount);
      }
    });
  });

  it('should sort items in collection by decreasing date', () => {
    const { result } = renderHook(() => useSort(txs));
      
    act(() => {
      result.current.setField('insertedAt');
      result.current.setDirection(-1);
    });
      
    result.current.collection.forEach((value, index, array) => {
      if (index < array.length-2) {
        expect(value.insertedAt).toBeGreaterThanOrEqual(array[index+1].insertedAt);
      }
    });
  });
});
