import { format, formatDistance } from "date-fns";
import { useState } from "react";

import { TxType } from "types/TxType";

export const getFilteredTxs = <T extends TxType>(filter: string, txs: T[]): T[] => {
  if (!filter) {
    return txs;
  }
  
  return txs.filter(tx => {
    const values = Object
      .values(tx)
      .filter(tx => Boolean(tx))
      .map(value => value.toString().toLowerCase() || value);

    return [
      format(new Date(tx.date), 'dd/MM/yyyy HH:mm:ss'),
      formatDistance(
        new Date(tx.date), 
        new Date()
      ),
      ...values
    ].some(
      value => value
        .toString()
        .startsWith(filter.toLowerCase())
    );
  });
};


export const useSearch = <T extends TxType>(collection: T[]) => {
  const [filter, setFilter] = useState('');

  return {
    collection: getFilteredTxs(filter, collection),
    filter,
    setFilter
  };
};
