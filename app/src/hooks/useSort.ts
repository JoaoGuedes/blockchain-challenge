import { useState } from "react";

export const ASCENDING = 1;

export const useSort = <T>(collection: T[]) => {
  const [field, setField] = useState<string>();
  const [direction, setDirection] = useState(ASCENDING);
  const sorted = collection.sort((a, b) => 
    a[field as keyof T] < b[field as keyof T] ? -direction : direction
  );

  return {
    collection: sorted,
    field: field as keyof T,
    direction,
    setField: (field: keyof T) => setField(field as string),
    setDirection,
  };
};
