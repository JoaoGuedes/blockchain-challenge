export type TX_TYPE = 'buy' | 'sell' | 'sent' | 'received'
export type TX_STATE = 'PENDING' | 'CONFIRMED' | 'FINISHED'

export interface BaseTx {
  state: TX_STATE
  type: TX_TYPE
}

export interface TxProps {
  amountCurrency: number
  date: number
}
  