import { BaseTx, TxProps } from "./Transaction";

export interface BitcoinDataTx extends BaseTx {
  amount: number
  blockHeight: number
  from: string
  to: string
  hash: string
  insertedAt: number
  txFee?: number
  txfee?: number
  rbf?: boolean
  data?: string
  description?: string
  coin: string
  double_spend: boolean
  fromWatchOnly: boolean
  toAddress: string
  toWatchOnly: boolean
}

export interface BitcoinTx extends BitcoinDataTx, TxProps {
  amountCoin: number
}

export interface BitcoinState {
  txs: BitcoinTx[]
  price: number
  loading: boolean
  error: Error | null
}