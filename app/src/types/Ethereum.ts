import { BaseTx, TxProps } from "./Transaction";

export interface EthereumDataTx extends BaseTx {
  amount: number
  blockHeight: string
  from: string
  to: string
  hash: string
  insertedAt: number
  txFee: string
  data?: string | null
  description?: string
  erc20: boolean
}

export interface EthereumTx extends EthereumDataTx, TxProps {
  amountCoin: number
}

export interface EthereumState {
  txs: EthereumTx[]
  price: number
  loading: boolean
  error: Error | null
}
  