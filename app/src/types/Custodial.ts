import { BaseTx, TxProps } from "./Transaction";

export interface CustodialDataTx extends BaseTx {
  id: string
  pair: string
  fiatValue: string
  fiatCurrency: string
  createdAt: string
  version: string
}

export interface CustodialTx extends CustodialDataTx, TxProps {}
  
export interface CustodialState {
  txs: CustodialTx[]
  loading: boolean
  error: Error | null
}
  