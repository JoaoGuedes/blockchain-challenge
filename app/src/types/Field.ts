export type Field<T> = {
  key: keyof T
  label: string
  className?: string
  sortable?: boolean
  omittable?: boolean
  render: (tx: T) => JSX.Element
}