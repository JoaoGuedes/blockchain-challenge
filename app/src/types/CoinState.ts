export interface CoinState<T> {
  txs: T[]
  price?: number
  loading: boolean
  error: Error | null
}