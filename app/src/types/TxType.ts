import { BitcoinTx } from "types/Bitcoin";
import { CustodialTx } from "types/Custodial";
import { EthereumTx } from "types/Ethereum";

export type TxType = BitcoinTx | EthereumTx | CustodialTx