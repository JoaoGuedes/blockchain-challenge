import { format, parseISO } from "date-fns";

import { FetchActionTypes } from "store/Fetch/types";
import { CustodialDataTx, CustodialState } from "types/Custodial";

import { 
  FETCH_REQUEST_CUSTODIAL,
  FETCH_ERROR_CUSTODIAL,
  FETCH_COMPLETE_CUSTODIAL
} from "./constants";


const initialState: CustodialState = {
  txs: [],
  loading: false,
  error: null
};

export default function reducer(state = initialState, action: FetchActionTypes): CustodialState {

  const props = {
    ...state,
    ...action.fetchProps
  };

  switch (action.type) {
    case FETCH_REQUEST_CUSTODIAL:
    case FETCH_ERROR_CUSTODIAL:
      return props;
    case FETCH_COMPLETE_CUSTODIAL:
      const txs: CustodialDataTx[] = action.payload;

      const mappedTxs = txs.map(tx => ({
        ...tx,
        amountCurrency: parseFloat(tx.fiatValue),
        date: parseInt(format(parseISO(tx.createdAt), 'T'))
      }));
      
      return {
        ...props,
        txs: mappedTxs
      };
    default:
      return state;
  }
}