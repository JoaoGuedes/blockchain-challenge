import { FetchError, FetchRequest, FetchComplete } from 'store/Fetch/types';
import { CustodialDataTx, CustodialState } from 'types/Custodial';

import { FETCH_COMPLETE_CUSTODIAL, FETCH_ERROR_CUSTODIAL, FETCH_REQUEST_CUSTODIAL } from './constants';
import reducer from './reducer';

const mockTx: CustodialDataTx = {
  id: "f2e12ed5-e22e-4c3a-8d4a-627a7386eb60",
  pair: "BTC-USD",
  state: "FINISHED",
  fiatValue: "51.70",
  fiatCurrency: "USD",
  version: "V2",
  type: "sell",
  createdAt: "2020-12-05T16:03:06.002Z"
};

describe('Custodial reducer', () => {
  it(`should return correct state for ${FETCH_REQUEST_CUSTODIAL}`, () => {
    const state: CustodialState = {
      txs: [],
      loading: true,
      error: null
    };  
    const action: FetchRequest = {
      type: FETCH_REQUEST_CUSTODIAL,
      fetchProps: {
        loading: true,
        error: null
      }
    };
    const expected: CustodialState = {
      ...state,
      ...action.fetchProps
    };

    expect(reducer(state, action)).toEqual(expected);
  });

  it(`should return correct state for ${FETCH_ERROR_CUSTODIAL}`, () => {
    const state: CustodialState = {
      txs: [],
      loading: true,
      error: null
    };  
    const action: FetchError = {
      type: FETCH_ERROR_CUSTODIAL,
      fetchProps: {
        loading: false,
        error: new Error('error')
      }
    };
    const expected: CustodialState = {
      ...state,
      ...action.fetchProps
    };

    expect(reducer(state, action)).toEqual(expected);
  });

  it(`should return correct state for ${FETCH_COMPLETE_CUSTODIAL}`, () => {
    const state: CustodialState = {
      txs: [],
      loading: true,
      error: null
    };  
    const action: FetchComplete = {
      type: FETCH_COMPLETE_CUSTODIAL,
      fetchProps: {
        loading: false,
        error: null
      },
      payload: [mockTx]
    };

    const expected: CustodialState = {
      ...state,
      ...action.fetchProps,
      txs: [
        {
          ...mockTx,
          amountCurrency: 51.7,
          date: 1607184186002,
        }
      ]
    };

    expect(reducer(state, action)).toEqual(expected);
  });
});