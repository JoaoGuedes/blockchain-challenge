export type FetchRequestCustodial = 'FETCH_REQUEST_CUSTODIAL'
export type FetchCompleteCustodial = 'FETCH_COMPLETE_CUSTODIAL'
export type FetchErrorCustodial = 'FETCH_ERROR_CUSTODIAL'