import { all, call, delay } from 'redux-saga/effects';

import { BTC_TXS_URL, ETH_TXS_URL, PRICES_URL, CUSTODIAL_TXS_URL, POLL_INTERVAL } from 'api';

import { FETCH_COMPLETE_BTC, FETCH_ERROR_BTC, FETCH_REQUEST_BTC } from './Bitcoin/constants';
import { FETCH_COMPLETE_CUSTODIAL, FETCH_ERROR_CUSTODIAL, FETCH_REQUEST_CUSTODIAL } from './Custodial/constants';
import { FETCH_COMPLETE_ETH, FETCH_ERROR_ETH, FETCH_REQUEST_ETH } from './Ethereum/constants';
import { fetchSaga } from './Fetch/saga';
import { FETCH_COMPLETE_PRICES, FETCH_ERROR_PRICES, FETCH_REQUEST_PRICES } from './Prices/constants';

export default function* rootSaga () {
  while (true) {
    yield all([
      call(
        fetchSaga,
        {
          actionTypes: {
            onRequest: FETCH_REQUEST_BTC,
            onComplete: FETCH_COMPLETE_BTC,
            onError: FETCH_ERROR_BTC
          },
          url: BTC_TXS_URL
        }
      ),
      call(
        fetchSaga,
        {
          actionTypes: {
            onRequest: FETCH_REQUEST_ETH,
            onComplete: FETCH_COMPLETE_ETH,
            onError: FETCH_ERROR_ETH
          },
          url: ETH_TXS_URL
        }
      ),
      call(
        fetchSaga,
        {
          actionTypes: {
            onRequest: FETCH_REQUEST_CUSTODIAL,
            onComplete: FETCH_COMPLETE_CUSTODIAL,
            onError: FETCH_ERROR_CUSTODIAL
          },
          url: CUSTODIAL_TXS_URL
        }
      ),
    ]);
    yield call(
      fetchSaga,
      {
        actionTypes: {
          onRequest: FETCH_REQUEST_PRICES,
          onComplete: FETCH_COMPLETE_PRICES,
          onError: FETCH_ERROR_PRICES
        },
        url: PRICES_URL
      }
    );
    yield delay(POLL_INTERVAL);
  }
}