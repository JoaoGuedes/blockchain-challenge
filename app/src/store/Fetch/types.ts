import { FetchRequestBTC, FetchCompleteBTC, FetchErrorBTC } from 'store/Bitcoin/types';
import { FetchRequestCustodial, FetchCompleteCustodial, FetchErrorCustodial } from 'store/Custodial/types';
import { FetchRequestETH, FetchCompleteETH, FetchErrorETH } from 'store/Ethereum/types';
import { FetchRequestPrices, FetchCompletePrices, FetchErrorPrices } from 'store/Prices/types';

interface FetchStateProps {
  loading: boolean
  error: Error | null
}

export interface FetchRequest {
  type: FetchRequestActionType
  fetchProps: FetchStateProps
  payload?: undefined
}

export interface FetchComplete {
  type: FetchCompleteActionType
  fetchProps: FetchStateProps
  payload: any
}

export interface FetchError {
  type: FetchErrorActionType
  fetchProps: FetchStateProps
  payload?: undefined
}

export type FetchActionTypes = FetchRequest | FetchComplete | FetchError
export type FetchRequestActionType = FetchRequestBTC | FetchRequestETH | FetchRequestCustodial | FetchRequestPrices
export type FetchCompleteActionType = FetchCompleteBTC | FetchCompleteETH | FetchCompleteCustodial | FetchCompletePrices
export type FetchErrorActionType = FetchErrorBTC | FetchErrorETH | FetchErrorCustodial | FetchErrorPrices
