import { call, put } from 'redux-saga/effects';

import { API } from 'api';

import { fetchSaga } from './saga';
import { FetchCompleteActionType, FetchErrorActionType, FetchRequestActionType } from './types';

const ON_REQUEST = 'ON_REQUEST' as FetchRequestActionType;
const ON_COMPLETE = 'ON_COMPLETE' as FetchCompleteActionType;
const ON_ERROR = 'ON_ERROR' as FetchErrorActionType;

const props = {
  actionTypes: {
    onRequest: ON_REQUEST,
    onComplete: ON_COMPLETE,
    onError: ON_ERROR
  },
  url: '/foo'
};

describe('fetchSaga', () => {
  it('should trigger request and complete actions', async () => {
    const generator = fetchSaga(props);

    expect(generator.next().value)
      .toEqual(
        put({
          type: ON_REQUEST,
          fetchProps: {
            loading: true,
            error: null
          }
        })
      );

    expect(generator.next().value).toEqual(call(API.get, '/foo'));

    expect(generator.next({ data: 'foo' }).value)
      .toEqual(
        put({
          type: ON_COMPLETE,
          payload: 'foo',
          fetchProps: {
            loading: false,
            error: null
          }
        })
      );

    expect(generator.next().done).toBeTruthy();
  });

  it('should trigger error action', async () => {
    const generator = fetchSaga(props);

    expect(generator.next().value)
      .toEqual(
        put({
          type: ON_REQUEST,
          fetchProps: {
            loading: true,
            error: null
          }
        })
      );

    expect(generator.next().value).toEqual(call(API.get, '/foo'));

    expect(generator.throw({ message: 'an error' }).value)
      .toEqual(
        put({
          type: ON_ERROR,
          fetchProps: {
            loading: false,
            error: {
              message: 'an error'
            }
          }
        })
      );
    expect(generator.next().done).toBeTruthy();
  });
});