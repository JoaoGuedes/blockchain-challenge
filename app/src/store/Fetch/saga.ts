import { call, put } from "redux-saga/effects";

import { API } from 'api';

import { complete, error, request } from "./actions";
import { FetchCompleteActionType, FetchErrorActionType, FetchRequestActionType } from "./types";

interface FetchProps {
  actionTypes: {
    onRequest: FetchRequestActionType
    onComplete: FetchCompleteActionType
    onError: FetchErrorActionType
  }
  url: string
}

export function* fetchSaga({
  actionTypes: {
    onRequest,
    onComplete,
    onError
  },
  url,
}: FetchProps) {
  yield put(request(onRequest));
  try {
    const { data } = yield call(API.get, url);
    yield put(complete(onComplete, data));
  } catch (err) {
    yield put(error(onError, err));
  }
}