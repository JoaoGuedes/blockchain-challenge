import { FetchActionTypes, FetchRequestActionType, FetchCompleteActionType, FetchErrorActionType } from "./types";

export const request = (type: FetchRequestActionType): FetchActionTypes => ({
  type,
  fetchProps: {
    loading: true,
    error: null
  }
});

export const complete = (type: FetchCompleteActionType, payload: any): FetchActionTypes => ({
  type,
  fetchProps: {
    loading: false,
    error: null
  },
  payload
});

export const error = (type: FetchErrorActionType, error: Error): FetchActionTypes => ({
  type,
  fetchProps: {
    loading: false,
    error: error
  },
});