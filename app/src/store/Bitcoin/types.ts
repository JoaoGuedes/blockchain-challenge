export type FetchRequestBTC = 'FETCH_REQUEST_BTC'
export type FetchCompleteBTC = 'FETCH_COMPLETE_BTC'
export type FetchErrorBTC = 'FETCH_ERROR_BTC'