import { FetchActionTypes } from "store/Fetch/types";
import { FETCH_COMPLETE_PRICES } from "store/Prices/constants";
import { BitcoinDataTx, BitcoinState, BitcoinTx } from 'types/Bitcoin';
import { fromSats, getPrice } from "utils";

import { FETCH_COMPLETE_BTC, FETCH_ERROR_BTC, FETCH_REQUEST_BTC } from "./constants";

const initialState: BitcoinState = {
  txs: [],
  price: 0,
  loading: false,
  error: null
};

export default function reducer(state = initialState, action: FetchActionTypes): BitcoinState {

  const props = {
    ...state,
    ...action.fetchProps
  };

  switch (action.type) {
    case FETCH_REQUEST_BTC:
    case FETCH_ERROR_BTC:
      return props;
    case FETCH_COMPLETE_BTC: {
      const txs: BitcoinDataTx[] = action.payload;

      const mappedTxs = txs.map(tx => ({
        ...tx,
        amountCurrency: getPrice(fromSats(tx.amount), state.price || 0),
        amountCoin: fromSats(tx.amount),
        date: tx.insertedAt * 1000
      }));
      
      return {
        ...props,
        txs: mappedTxs
      };
    }
    case FETCH_COMPLETE_PRICES:
      const price: number = action.payload.BTC;
      const txs: BitcoinTx[] = state.txs;

      const mappedTxs = txs.map(tx => ({
        ...tx,
        txFee: tx.txFee || tx.txfee,
        amountCurrency: getPrice(fromSats(tx.amount), price)
      }));
      
      return {
        ...props,
        txs: mappedTxs,
        price
      };
    default:
      return state;
  }
}