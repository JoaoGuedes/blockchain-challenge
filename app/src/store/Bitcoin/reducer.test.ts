import { FetchError, FetchRequest, FetchComplete } from 'store/Fetch/types';
import { FETCH_COMPLETE_PRICES } from 'store/Prices/constants';
import { BitcoinDataTx, BitcoinState, BitcoinTx } from 'types/Bitcoin';

import { FETCH_COMPLETE_BTC, FETCH_ERROR_BTC, FETCH_REQUEST_BTC } from './constants';
import reducer from './reducer';

const mockTx: BitcoinDataTx = {
  amount: 212903,
  blockHeight: 661982,
  coin: 'BTC',
  description: '',
  double_spend: false,
  from: 'My Bitcoin Wallet',
  fromWatchOnly: false,
  hash: '12c645ba3c11ad4383e43896da18087cc1dce791e0f1c0c0b79bbf168b0dc266',
  insertedAt: 1608318771,
  state: 'CONFIRMED',
  to: '1HyCCuBN4otT5utzD8Km3G3zjUtAjucF3u',
  toAddress: '1HyCCuBN4otT5utzD8Km3G3zjUtAjucF3u',
  toWatchOnly: false,
  txFee: 33496,
  type: 'sent',
};

const mockOutputTx: BitcoinTx = {
  ...mockTx,
  amountCoin: 0,
  amountCurrency: 0,
  date: 0
};

describe('Bitcoin reducer', () => {
  it(`should return correct state for ${FETCH_REQUEST_BTC}`, () => {
    const state: BitcoinState = {
      txs: [],
      price: 0,
      loading: true,
      error: null
    };  
    const action: FetchRequest = {
      type: FETCH_REQUEST_BTC,
      fetchProps: {
        loading: true,
        error: null
      }
    };
    const expected: BitcoinState = {
      ...state,
      ...action.fetchProps
    };

    expect(reducer(state, action)).toEqual(expected);
  });

  it(`should return correct state for ${FETCH_ERROR_BTC}`, () => {
    const state: BitcoinState = {
      txs: [],
      price: 0,
      loading: true,
      error: null
    };  
    const action: FetchError = {
      type: FETCH_ERROR_BTC,
      fetchProps: {
        loading: false,
        error: new Error('error')
      }
    };
    const expected: BitcoinState = {
      ...state,
      ...action.fetchProps
    };

    expect(reducer(state, action)).toEqual(expected);
  });

  it(`should return correct state for ${FETCH_COMPLETE_BTC}`, () => {
    const state: BitcoinState = {
      txs: [],
      price: 0,
      loading: true,
      error: null
    };  
    const action: FetchComplete = {
      type: FETCH_COMPLETE_BTC,
      fetchProps: {
        loading: false,
        error: null
      },
      payload: [mockTx]
    };

    const expected: BitcoinState = {
      ...state,
      ...action.fetchProps,
      txs: [
        {
          ...mockTx,
          amountCurrency: 0,
          amountCoin: 0.00212903,
          date: mockTx.insertedAt * 1000
        }
      ]
    };

    expect(reducer(state, action)).toEqual(expected);
  });

  it(`should return correct state for ${FETCH_COMPLETE_PRICES}`, () => {
    const state: BitcoinState = {
      txs: [mockOutputTx],
      price: 0,
      loading: true,
      error: null
    };  
    const action: FetchComplete = {
      type: FETCH_COMPLETE_PRICES,
      fetchProps: {
        loading: false,
        error: null
      },
      payload: {
        BTC: 30000
      }
    };

    const expected: BitcoinState = {
      ...state,
      ...action.fetchProps,
      price: 30000,
      txs: [
        {
          ...mockOutputTx,
          amountCurrency: 63.87
        }
      ]
    };

    expect(reducer(state, action)).toEqual(expected);
  });
});