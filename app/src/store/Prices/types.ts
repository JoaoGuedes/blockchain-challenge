export type FetchRequestPrices = 'FETCH_REQUEST_PRICES'
export type FetchCompletePrices = 'FETCH_COMPLETE_PRICES'
export type FetchErrorPrices = 'FETCH_ERROR_PRICES'
