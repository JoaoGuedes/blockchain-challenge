import { FetchError, FetchRequest, FetchComplete } from 'store/Fetch/types';
import { FETCH_COMPLETE_PRICES } from 'store/Prices/constants';
import { EthereumState, EthereumDataTx, EthereumTx } from 'types/Ethereum';

import { FETCH_COMPLETE_ETH, FETCH_ERROR_ETH, FETCH_REQUEST_ETH } from './constants';
import reducer from './reducer';

const mockTx: EthereumDataTx = {
  amount: 19313120320400000,
  blockHeight: "10561201",
  data: null,
  description: "Pineapples belong on pizza",
  erc20: false,
  from: "My Ether Wallet",
  hash: "0x335f01c4b5cccda2b24708a42337cafa3628a888792d4b40f0aef3499a281eca",
  insertedAt: 1596115731,
  state: "PENDING",
  to: "0x732904f98f9bd820c643331ec48d2ebce1e52c2f",
  txFee: "1464400000000",
  type: "sent"
};

const mockOutputTx: EthereumTx = {
  ...mockTx,
  amountCoin: 0,
  amountCurrency: 0,
  date: 0
};

describe('Ethereum reducer', () => {
  it(`should return correct state for ${FETCH_REQUEST_ETH}`, () => {
    const state: EthereumState = {
      txs: [],
      price: 0,
      loading: true,
      error: null
    };  
    const action: FetchRequest = {
      type: FETCH_REQUEST_ETH,
      fetchProps: {
        loading: true,
        error: null
      }
    };
    const expected: EthereumState = {
      ...state,
      ...action.fetchProps
    };

    expect(reducer(state, action)).toEqual(expected);
  });

  it(`should return correct state for ${FETCH_ERROR_ETH}`, () => {
    const state: EthereumState = {
      txs: [],
      price: 0,
      loading: true,
      error: null
    };  
    const action: FetchError = {
      type: FETCH_ERROR_ETH,
      fetchProps: {
        loading: false,
        error: new Error('error')
      }
    };
    const expected: EthereumState = {
      ...state,
      ...action.fetchProps
    };

    expect(reducer(state, action)).toEqual(expected);
  });

  it(`should return correct state for ${FETCH_COMPLETE_ETH}`, () => {
    const state: EthereumState = {
      txs: [],
      price: 0,
      loading: true,
      error: null
    };  
    const action: FetchComplete = {
      type: FETCH_COMPLETE_ETH,
      fetchProps: {
        loading: false,
        error: null
      },
      payload: [mockTx]
    };

    const expected: EthereumState = {
      ...state,
      ...action.fetchProps,
      txs: [
        {
          ...mockTx,
          amountCurrency: 0,
          amountCoin: 0.01931312,
          date: mockTx.insertedAt * 1000
        }
      ]
    };

    expect(reducer(state, action)).toEqual(expected);
  });

  it(`should return correct state for ${FETCH_COMPLETE_PRICES}`, () => {
    const state: EthereumState = {
      txs: [mockOutputTx],
      price: 0,
      loading: true,
      error: null
    };  
    const action: FetchComplete = {
      type: FETCH_COMPLETE_PRICES,
      fetchProps: {
        loading: false,
        error: null
      },
      payload: {
        ETH: 30000
      }
    };

    const expected: EthereumState = {
      ...state,
      ...action.fetchProps,
      price: 30000,
      txs: [
        {
          ...mockOutputTx,
          amountCurrency: 579.39
        }
      ]
    };

    expect(reducer(state, action)).toEqual(expected);
  });
});