import { FetchActionTypes } from "store/Fetch/types";
import { FETCH_COMPLETE_PRICES } from "store/Prices/constants";
import { EthereumDataTx, EthereumState, EthereumTx } from "types/Ethereum";
import { fromWeis, getPrice } from "utils";

import { FETCH_COMPLETE_ETH, FETCH_ERROR_ETH, FETCH_REQUEST_ETH } from "./constants";

const initialState: EthereumState = {
  txs: [],
  price: 0,
  loading: false,
  error: null
};

export default function reducer(state = initialState, action: FetchActionTypes): EthereumState {

  const props = {
    ...state,
    ...action.fetchProps
  };

  switch (action.type) {
    case FETCH_REQUEST_ETH:
    case FETCH_ERROR_ETH:
      return props;
    case FETCH_COMPLETE_ETH: {
      const txs: EthereumDataTx[] = action.payload;

      const mappedTxs = txs.map(tx => ({
        ...tx,
        amountCurrency: getPrice(fromWeis(tx.amount), state.price || 0),
        amountCoin: fromWeis(tx.amount),
        date: tx.insertedAt * 1000
      }));
      
      return {
        ...props,
        txs: mappedTxs
      };
    }
    case FETCH_COMPLETE_PRICES:
      const price: number = action.payload.ETH;
      const txs: EthereumTx[] = state.txs;

      const mappedTxs = txs.map(tx => ({
        ...tx,
        amountCurrency: getPrice(fromWeis(tx.amount), price)
      }));
      
      return {
        ...props,
        txs: mappedTxs,
        price
      };  
    default:
      return state;
  }
}