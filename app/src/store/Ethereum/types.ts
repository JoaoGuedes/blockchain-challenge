export type FetchRequestETH = 'FETCH_REQUEST_ETH'
export type FetchCompleteETH = 'FETCH_COMPLETE_ETH'
export type FetchErrorETH = 'FETCH_ERROR_ETH'