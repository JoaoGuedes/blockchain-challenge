import { combineReducers } from 'redux';

import { reducer as bitcoinReducer } from './Bitcoin';
import { reducer as custodialReducer } from './Custodial';
import { reducer as ethereumReducer } from './Ethereum';

const rootReducer = combineReducers({
  bitcoin: bitcoinReducer,
  ethereum: ethereumReducer,
  custodial: custodialReducer
});

export type RootState = ReturnType<typeof rootReducer>

export default rootReducer;