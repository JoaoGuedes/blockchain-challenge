import { ChangeEvent } from "react";

import { TxType } from "types/TxType";

interface Props<T> {
  field?: keyof T
  direction: number
  onChange: (field: string, direction: number) => void
}
  
export const SortDropdown = <T extends TxType>({ field, direction, onChange: onSort }: Props<T>) => {
  const onChange = (event: ChangeEvent<HTMLSelectElement>) => {
    const [field, direction] = event.target.value.split(',');
    onSort(field, parseInt(direction));
  };
    
  return (
    <div className="flex justify-end">
      <select onChange={onChange} value={`${field},${direction}`} className="w-40 text-sm p-2 font-light mr-2">
        <option value="undefined,1" disabled>Sort by</option>
        <option value="insertedAt,1">Date (older first)</option>
        <option value="insertedAt,-1">Date (recent first)</option>
        <option value="amount,1">Amount (asc.)</option>
        <option value="amount,-1">Amount (desc.)</option>
        <option value="type,1">Type (asc.)</option>
        <option value="type,-1">Type (desc.)</option>
        <option value="state,1">Status (asc.)</option>
        <option value="state,-1">Status (desc.)</option>
      </select>
    </div>
  );
};