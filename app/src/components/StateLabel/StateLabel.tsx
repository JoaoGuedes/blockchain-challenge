import { TX_STATE } from "types/Transaction";


interface Props {
  value: TX_STATE
}

export const StateLabel = ({ value }: Props) => {
  let colorProps;

  if  (['CONFIRMED', 'FINISHED'].includes(value)) {
    colorProps = `bg-green-100 text-green-600`;
  }

  if (value === 'PENDING') {
    colorProps = `bg-yellow-100 text-yellow-600`;
  }
  
  return (
    <span className={`${colorProps} text-xs py-1 px-2 rounded-full font-semibold shadow-sm`}>
      {value}
    </span>
  );
};