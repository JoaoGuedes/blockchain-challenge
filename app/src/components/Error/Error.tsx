import { ReactNode } from "react";

interface Props {
  children: ReactNode
}

export const Error = ({ children }: Props) => (
  <div className="w-full h-96 flex justify-center items-center bg-gray-100 rounded-sm my-4">    
    <svg className="h-6 w-6 inline mx-2" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
      <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M12 8v4m0 4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
    </svg>
    <span className="text-lg">{ children }</span>
  </div>
);