import { ReactNode } from "react";

interface Props {
  label: string
  price?: string
  Spinner: ReactNode
  SearchInput: ReactNode
}

export const Header = ({ label, price, Spinner, SearchInput }: Props) => (
  <header className="my-4 flex justify-between items-center flex-col sm:flex-row sticky top-0 z-20 bg-white pb-3 lg:pb-0 lg:static">
    <div className="hidden sm:w-96 sm:block"/>
    <div className="flex items-center justify-center w-full sm:w-auto">
      <span className="text-center py-4 font-bold mx-2">{ label }</span>
      { price && (
        <span className="bg-blue-500 hover:bg-blue-700 text-white text-xs py-1 px-2 rounded-full">
          { price }
        </span>
      )}
      {Spinner}
    </div>
    <div className="w-full px-4 sm:w-auto sm:px-0">
      {SearchInput}
    </div>
  </header>
);