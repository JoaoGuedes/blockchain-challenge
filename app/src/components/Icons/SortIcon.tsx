import { ASCENDING } from "hooks";

import { SortAscending } from "./SortAscending";
import { SortDescending } from "./SortDescending";

interface Props {
  direction: number
  visible: boolean
}

export const SortIcon = ({ direction, visible }: Props) => {
  if (direction === ASCENDING) {
    return <SortAscending className={`${visible ? 'opacity-100' : 'opacity-25 hover:opacity-75'} mx-1`} />;
  }
  return <SortDescending className={`${visible ? 'opacity-100' : 'opacity-25 hover:opacity-75'} mx-1`}/>;
};