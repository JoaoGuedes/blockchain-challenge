import React from 'react';

import { TX_TYPE } from "types/Transaction";

import { LeftArrow } from "./LeftArrow";
import { RightArrow } from "./RightArrow";

interface Props {
  type: TX_TYPE
}  

export const TransferIcon = ({ type }: Props) => {
  if (type === 'received') {
    return <RightArrow />;
  }
  if (type === 'sent') {
    return <LeftArrow />;
  }
  return null;
};