import { formatDistance } from 'date-fns';

import { TransferIcon } from 'components/Icons';
import { Field } from 'types/Field';
import { TxType } from 'types/TxType';

interface Props<T> {
  tx: T
  fields: Field<T>[]
}

export const TxBlock = <T extends TxType>({ tx, fields }: Props<T>) => {
  const bgColor = tx.state === 'PENDING' ? 'bg-yellow-50' : 'bg-green-50';
  const borderColor = tx.state === 'PENDING' ? 'border-yellow-100' : 'border-green-100';
  const txtColorLight = tx.state === 'PENDING' ? 'text-yellow-500' : 'text-green-500';
  const txtColor = tx.state === 'PENDING' ? 'text-yellow-700' : 'text-green-700';
  
  const date = formatDistance(
    new Date(tx.date), 
    new Date()
  );

  const invertedType = ['sent', 'sell'].includes(tx.type) ? 'received' : 'sent';

  return (
    <div className={`px-5 pt-14 pb-4 my-4 ${bgColor} relative shadow-sm rounded-sm border ${borderColor}`}>
      {fields.map(({ label, key, render = () => null}) => (
        <div key={key as string} className="flex items-baseline">
          <p key={label} className={`text-sm text-right ${txtColorLight} my-2 mr-2 w-16 flex-shrink-0`}>{ label }:</p>
          <div key={key as string} className="flex items-baseline my-2 truncate">{render(tx)}</div>
        </div>
      ))}
      <div className="absolute top-0 right-0 p-2 truncate flex items-center justify-center">
        <span className={`${txtColor} text-xs ${bgColor} p-2 rounded-full`}>{date} ago</span>
        <div className={`rounded-full mx-2 p-1 ${borderColor} border `}>
          <TransferIcon type={invertedType}/>
        </div>
      </div>
    </div>
  );
};