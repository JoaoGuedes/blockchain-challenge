import { repeat } from "utils";

interface Props {
  count: number
}

export const TxBlocksSkeleton = ({ count }: Props) => (
  <>
    {
      repeat(count).map((v, i) => <div key={`tx-block-skeleton-${i}`} className="h-96 w-full bg-gray-100 rounded-sm animate-pulse my-4" />)
    }
  </>
);