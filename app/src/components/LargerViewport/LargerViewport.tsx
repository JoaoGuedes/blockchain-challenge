import { ReactElement, ReactNode } from 'react';
import useMedia from 'use-media';

interface Props {
  children: ReactNode
}

export const LargerViewport = ({ children }: Props) => {
  const isLarge = useMedia({ minWidth: '1024px' });
  return isLarge ? children as ReactElement : null;
};
  