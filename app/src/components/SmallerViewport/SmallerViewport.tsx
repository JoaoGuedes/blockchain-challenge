import { ReactElement, ReactNode } from 'react';
import useMedia from 'use-media';

interface Props {
  children: ReactNode
}

export const SmallerViewport = ({ children }: Props) => {
  const isLarge = useMedia({ minWidth: '1024px' });
  return isLarge ? null : children as ReactElement;
};
  