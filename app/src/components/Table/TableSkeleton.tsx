import { repeat } from 'utils';

interface TableSkeletonProps {
  rows: number
  columns: number
}

interface TableRowSkeletonProps {
  columns: number
}

const TableRowSkeleton = ({ columns }: TableRowSkeletonProps) => (
  <div className="w-full flex">
    {
      repeat(columns).map((v, i) => (
        <div className="h-10 p-3 rounded" key={`table-row-cell-${i}`} style={{ width: `calc(100% / ${columns})` }}>
          <div className="h-full bg-gray-200 animate-pulse" style={{ width: `${Math.random() * 100}%` }}/>
        </div>
      ))
    }
  </div>
);
    
export const TableSkeleton = ({ rows, columns }: TableSkeletonProps) => (
  <div className="h-96 shadow overflow-hidden border-b border-gray-200 rounded-lg">
    {
      repeat(rows).map((v, i) => <TableRowSkeleton key={`table-row-skeleton-${i}`} columns={columns}/>)
    }
  </div>
);