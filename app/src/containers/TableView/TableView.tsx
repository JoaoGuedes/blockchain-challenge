import { Error } from 'components/Error';
import { Header } from 'components/Header';
import { SortIcon, Spinner , TransferIcon } from 'components/Icons';
import { LargerViewport } from 'components/LargerViewport';
import { SearchInput } from 'components/SearchInput';
import { SmallerViewport } from 'components/SmallerViewport';
import { SortDropdown } from 'components/SortDropdown';
import { TableSkeleton } from 'components/Table/TableSkeleton';
import { TxBlock, TxBlocksSkeleton } from 'components/TxBlock';
import { useSort, useSearch } from 'hooks';
import { CoinState } from 'types/CoinState';
import { Field } from 'types/Field';
import { TxType } from 'types/TxType';
import { format } from 'utils';

interface Props<T> {
  state: CoinState<T>
  fields: Field<T>[]
  label: string
  generateTxKey: (tx: T) => string
}

export const TableView = <T extends TxType>({ state, fields, label, generateTxKey = () => '' }: Props<T>) => {
  const { txs, price, loading, error } = state;
  const { collection: filteredTxs, filter, setFilter } = useSearch(txs);
  const { collection: sortedTxs, direction, field: sortField, setField, setDirection } = useSort(filteredTxs);

  const onSort = (field: keyof T, direction: number) => {
    setField(field);
    setDirection(direction);
  };

  const hasTxs = !!sortedTxs.length;
  const isInitialLoad = !filter && loading && !hasTxs;

  if (error && !hasTxs) {
    return <Error>Error loading {label} data</Error>;
  }

  const viewableFields = fields.filter(field => !field.omittable);

  return (
    <section className="flex flex-col py-4" >
      <Header 
        label={label}
        price={price ? format(price) : undefined}
        Spinner={<Spinner className={loading ? 'visible' : 'invisible'} />}
        SearchInput={<SearchInput value={filter} onChange={(value: string) => setFilter(value)} />}
      />
      <article>

        <SmallerViewport>
          <SortDropdown field={sortField} direction={direction} onChange={(field, direction) => onSort(field as keyof T, direction)}/>
          {isInitialLoad && <TxBlocksSkeleton count={12}/>}
          {hasTxs && sortedTxs.map((tx) => <TxBlock key={generateTxKey(tx)} tx={tx} fields={fields} />)}
        </SmallerViewport>

        <LargerViewport>
          { isInitialLoad && <TableSkeleton columns={viewableFields.length} rows={12}/> }
          <div className="flex flex-col shadow overflow-hidden border-b border-gray-200 rounded-lg">
            { !isInitialLoad && <div className="flex w-full bg-gradient-to-b from-gray-100 to-gray-300">
              <div className="w-10 p-3">&nbsp;</div>
              {viewableFields
                .map(({ key, label, sortable, className = 'w-1/12' }) => (
                  <div key={label} 
                    className={`${sortable ? 'cursor-pointer' : ''} ${className} mr-2 px-3 py-4 text-xs font-medium text-gray-500 uppercase tracking-wider`}
                    onClick={() => sortable ? onSort(key, -direction) : null}>
                    <span>{label}</span>
                    {sortable && <SortIcon direction={direction} visible={sortField === key} /> }
                  </div>     
                ))}
            </div> }
            <div className="h-96 overflow-y-scroll overflow-x-hidden">
              { hasTxs && sortedTxs.map(tx => (
                <div key={generateTxKey(tx)} className="flex border-b items-center">
                  <>
                    <div className="w-10 p-3">
                      { tx.type && <TransferIcon type={tx.type}/> }
                    </div>
                    {fields
                      .filter(field => !field.omittable)
                      .map(({ key, render, className = 'w-1/12' }) => (
                        <div key={key as string} className={`${className} mr-2 p-3`}>
                          <div className="truncate">{render(tx)}</div>
                        </div>     
                      ))}
                  </>
                </div>            
              )) }
            </div>
          </div>
        </LargerViewport>
    
      </article>
    </section>
  );
};
