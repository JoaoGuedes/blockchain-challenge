import { format, formatDistance } from 'date-fns';
import { useSelector } from 'react-redux';

import { StateLabel } from 'components/StateLabel';
import { TableView } from 'containers/TableView';
import { RootState } from 'store/rootReducer';
import { CustodialTx } from 'types/Custodial';
import { Field } from 'types/Field';
import { format as formatCurrency } from 'utils';

const CustodialFields: Field<CustodialTx>[] = [
  {
    key: 'id',
    label: 'ID',
    className: 'w-1/4',
    sortable: true,
    render: tx => <span className="font-light text-xs font-mono">{tx.id}</span>
  },
  {
    key: 'pair',
    label: 'Pair',
    className: 'w-1/12',
    sortable: true,
    render: tx => <span className="font-light text-sm">{tx.pair}</span>
  },
  {
    key: 'amountCurrency',
    className: 'w-2/12 text-right',
    label: 'Amount',
    sortable: true,
    render: tx => <p className="text-sm text-gray-800">{formatCurrency(tx.amountCurrency, tx.fiatCurrency)}</p>
  },
  {
    key: 'version',
    label: 'Version',
    className: 'w-2/12',
    sortable: true,
    render: tx => <span className="font-light text-xs font-mono">{tx.version}</span>
  },
  {
    key: 'type',
    label: 'Type',
    className: 'w-2/12',
    sortable: true,
    render: tx => <span className="font-light text-sm">{tx.type}</span>
  },
  {
    key: 'state',
    label: 'Status',
    className: 'w-2/12',
    sortable: true,
    render: tx => <StateLabel value={tx.state} />
  },
  {
    key: 'date',
    className: 'text-right w-2/12',
    label: 'Date',
    sortable: true,
    render: tx => (
      <>
        <p className="font-light text-sm text-gray-600">{format(new Date(tx.date), 'dd/MM/yyyy HH:mm:ss')}</p>
        <p className="font-normal text-xs ml-1 hidden lg:block">{formatDistance(
          new Date(tx.date), 
          new Date()
        )} ago</p>
      </>
    )
  },
];

const generateTxKey = (tx: CustodialTx) => tx.id;

export const CustodialView = () => {
  const CustodialState = useSelector((state: RootState) => state.custodial);
  return <TableView label="Custodial" state={CustodialState} fields={CustodialFields} generateTxKey={generateTxKey} />;
};