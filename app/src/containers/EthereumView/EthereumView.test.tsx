import { render, fireEvent } from '@testing-library/react';
import { format } from 'date-fns';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store'; //ES6 modules

import { txs as dataTxs } from 'api/mocks/ethereumTxs';
import { useSort, useSearch } from 'hooks';
import { sagaMiddleware } from 'store';
import { EthereumTx } from 'types/Ethereum';
import { jestMockMatchMedia, format as formatCurrency, getPrice, fromWeis } from 'utils';

import { EthereumView } from "./EthereumView";

const mediaQueries = {
  largeViewport: '(min-width: 1024px)',
};

const middlewares = [sagaMiddleware];
const mockStore = configureStore(middlewares);
let store : ReturnType<typeof mockStore>;

jest.mock('hooks');

const txs = dataTxs.map(tx => ({
  ...tx,
  amountCurrency: getPrice(fromWeis(tx.amount), 1000),
  amountCoin: fromWeis(tx.amount),
  date: tx.insertedAt * 1000
}));

describe('<EthereumView>', () => {

  beforeEach(() => {
    store = mockStore({
      ethereum: {
        txs,
        price: 1000,
        loading: false,
        error: null
      }
    });
    (useSort as jest.Mock).mockImplementation(() => ({
      collection: txs,
      setField: jest.fn(),
      setDirection: jest.fn()
    }));
    (useSearch as jest.Mock).mockImplementation(() => ({
      collection: txs,
      setFilter: jest.fn()
    }));
  });

  it('should call sorting function', () => {
    const spy = jest.fn();
    (useSort as jest.Mock).mockImplementation(() => ({
      collection: txs,
      setField: spy,
      setDirection: jest.fn()
    }));
    jestMockMatchMedia({
      media: mediaQueries.largeViewport,
      matches: true,
    });
    const { getByText } = render(
      <Provider store={store}>
        <EthereumView />
      </Provider>
    );
    fireEvent.click(getByText('Amount'));
    expect(spy).toHaveBeenCalled();
  });

  it('should call search function', () => {
    const spy = jest.fn();
    (useSearch as jest.Mock).mockImplementation(() => ({
      collection: txs,
      setFilter: spy
    }));
    jestMockMatchMedia({
      media: mediaQueries.largeViewport,
      matches: true,
    });
    const { getByPlaceholderText } = render(
      <Provider store={store}>
        <EthereumView />
      </Provider>
    );
    fireEvent.change(getByPlaceholderText('Search'), { target: { value: 'foo' }});
    expect(spy).toHaveBeenCalled();
  });

  it('should display correct fields on larger viewport', () => {
    jestMockMatchMedia({
      media: mediaQueries.largeViewport,
      matches: true,
    });
    const { queryByText, queryAllByText } = render(
      <Provider store={store}>
        <EthereumView />
      </Provider>
    );
    const [tx] = txs as EthereumTx[];
    expect(queryByText('ETH')).toBeInTheDocument();
    expect(queryByText(formatCurrency(1000))).toBeInTheDocument();
    expect(queryAllByText(tx.from)[0]).toBeInTheDocument();
    expect(queryAllByText(tx.to)[0]).toBeInTheDocument();
    expect(queryAllByText(`${tx.amountCoin} ETH`)[0]).toBeInTheDocument();
    expect(queryAllByText(formatCurrency(tx.amountCurrency))[0]).toBeInTheDocument();
    expect(queryAllByText(tx.type)[0]).toBeInTheDocument();
    expect(queryAllByText(tx.state)[0]).toBeInTheDocument();
    expect(queryAllByText(format(new Date(tx.date), 'dd/MM/yyyy HH:mm:ss'))[0]).toBeInTheDocument();
  });

  it('should display correct fields on smaller viewport', () => {
    jestMockMatchMedia({
      media: mediaQueries.largeViewport,
      matches: false,
    });
    const store = mockStore({
      ethereum: {
        txs,
        price: 1000,
        loading: false,
        error: null
      }
    });

    const { queryByText, queryAllByText } = render(
      <Provider store={store}>
        <EthereumView />
      </Provider>
    );
    const [tx] = txs as EthereumTx[];
    expect(queryByText('ETH')).toBeInTheDocument();
    expect(queryByText(formatCurrency(1000))).toBeInTheDocument();
    expect(queryAllByText(tx.from)[0]).toBeInTheDocument();
    expect(queryAllByText(tx.to)[0]).toBeInTheDocument();
    expect(queryAllByText(`${tx.amountCoin} ETH`)[0]).toBeInTheDocument();
    expect(queryAllByText(formatCurrency(tx.amountCurrency))[0]).toBeInTheDocument();
    expect(queryAllByText(tx.type)[0]).toBeInTheDocument();
    expect(queryAllByText(tx.state)[0]).toBeInTheDocument();
    expect(queryAllByText(format(new Date(tx.date), 'dd/MM/yyyy HH:mm:ss'))[0]).toBeInTheDocument();
    expect(queryAllByText(tx.blockHeight)[0]).toBeInTheDocument();
    expect(queryAllByText(tx.hash)[0]).toBeInTheDocument();
    expect(queryAllByText(tx.description || '')[0]).toBeInTheDocument();
    expect(queryAllByText(`${fromWeis(parseInt(`${tx.txFee}`))} ETH`)[0]).toBeInTheDocument();
  });

  
});