import { Provider,  } from 'react-redux';

import { BitcoinView } from 'containers/BitcoinView';
import { CustodialView } from 'containers/CustodialView';
import { EthereumView } from 'containers/EthereumView';
import store from 'store';

export const App = () => {
  return (
    <Provider store={store}>
      <div className="container mx-auto py-8">
        <BitcoinView />
        <EthereumView />
        <CustodialView />
      </div>
    </Provider>
  );
};
