import { format, formatDistance } from 'date-fns';
import { useSelector } from 'react-redux';

import { StateLabel } from 'components/StateLabel';
import { TableView } from 'containers/TableView';
import { RootState } from 'store/rootReducer';
import { BitcoinTx } from 'types/Bitcoin';
import { Field } from 'types/Field';
import { format as formatCurrency, fromSats } from 'utils';

const bitcoinFields: Field<BitcoinTx>[] = [
  {
    key: 'from',
    label: 'From',
    className: 'w-3/12',
    sortable: true,
    render: tx => <span className="font-light text-xs font-mono truncate">{tx.from}</span>
  },
  {
    key: 'to',
    label: 'To',
    className: 'w-3/12',
    sortable: true,
    render: tx => <span className="font-light text-xs font-mono truncate">{tx.to}</span>
  },
  {
    key: 'amount',
    className: 'w-40 text-right',
    label: 'Amount',
    sortable: true,
    render: tx => (
      <>
        <p className="text-sm text-gray-800">{tx.amountCoin} <span className="text-xs">{tx.coin}</span></p>
        <p className="text-xs font-normal text-gray-400 ml-1">{formatCurrency(tx.amountCurrency)}</p>
      </>
    )
  },
  {
    key: 'txFee',
    label: 'Fee',
    sortable: true,
    omittable: true,
    render: tx => <p className="text-sm text-gray-800">{tx.txFee ? `${fromSats(parseInt(`${tx.txFee}`))} BTC` :  'N/A'}</p>
  },
  {
    key: 'hash',
    label: 'Hash',
    sortable: true,
    omittable: true,
    render: tx => <span className="font-light text-xs font-mono truncate">{tx.hash}</span>
  },
  {
    key: 'blockHeight',
    label: 'Block',
    sortable: true,
    omittable: true,
    render: tx => <span className="font-light text-sm">{tx.blockHeight}</span>
  },
  {
    key: 'type',
    label: 'Type',
    sortable: true,
    render: tx => <span className="font-light text-sm">{tx.type}</span>
  },
  {
    key: 'state',
    label: 'Status',
    sortable: true,
    render: tx => <StateLabel value={tx.state} />
  },
  {
    key: 'insertedAt',
    className: 'text-right w-2/12',
    label: 'Date',
    sortable: true,
    render: tx => (
      <>
        <p className="font-light text-sm text-gray-600">{format(new Date(tx.date), 'dd/MM/yyyy HH:mm:ss')}</p>
        <p className="font-normal text-xs ml-1 hidden lg:block">{formatDistance(
          new Date(tx.date), 
          new Date()
        )} ago</p>
      </>
    )
  },
];

const generateTxKey = (tx: BitcoinTx) => tx.hash;

export const BitcoinView = () => {
  const bitcoinState = useSelector((state: RootState) => state.bitcoin);
  return <TableView label="BTC" state={bitcoinState} fields={bitcoinFields} generateTxKey={generateTxKey}/>;
};