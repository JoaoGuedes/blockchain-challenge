import {mockMediaQueryList} from 'use-media/lib/useMedia';
// Types are also exported for convienence:
// import {Effect, MediaQueryObject} from 'use-media/lib/types';

export interface MockMatchMedia {
  media: string;
  matches?: boolean;
}

const getMockImplementation = ({media, matches = false}: MockMatchMedia) => {
  const mql: MediaQueryList = {
    ...mockMediaQueryList,
    media,
    matches,
  };

  return () => mql;
};

export const jestMockMatchMedia = ({media, matches = false}: MockMatchMedia) => {
  const mockedImplementation = getMockImplementation({media,
    matches});
  window.matchMedia = jest.fn().mockImplementation(mockedImplementation);
};

export const fromSats = (value: number) => parseFloat((value * Math.pow(10, -8)).toFixed(8));

export const fromWeis = (value: number) => parseFloat((value * Math.pow(10, -18)).toFixed(8));

export const getPrice = (amount: number, price: number) => parseFloat((amount*price).toFixed(2));

export const repeat = (times: number) => ' '.repeat(times).split('');

export const format = (amount: number, currency?: string) => {
  const formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: currency || 'USD',
  });

  return formatter.format(amount);
};