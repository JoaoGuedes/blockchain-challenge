import { fromSats, fromWeis, format, repeat, getPrice } from "utils";


describe('utils', () => {
  it.each`
      input             | result
      ${383858353}      | ${3.83858353}
      ${74927492}       | ${0.74927492}
    `('should convert SAT with value $input to $result',
    ({ input, result }) => expect(fromSats(input)).toEqual(result));

  it.each`
    input                       | result
    ${3019313120320400000}      | ${3.01931312}
    ${23544441334}              | ${0.00000002}
  `('should convert WEI with value $input to $result',
    ({ input, result }) => expect(fromWeis(input)).toEqual(result));

  it.each`
    input       | price         | result
    ${122}      | ${343}        | ${41846}
    ${10}       | ${0.1}        | ${1}
  `('should multiply price $price by $input to $result',
    ({ input, price, result }) => expect(getPrice(input, price)).toEqual(result));

  it.each`
    input          | result
    ${12234}       | ${'$12,234.00'}
    ${0.03}        | ${'$0.03'}
  `('should format price with value $input to $result',
    ({ input, result }) => expect(format(input)).toEqual(result));

  it.each`
    input          | result
    ${4}           | ${[' ',' ',' ',' ']}
    ${0}           | ${[]}
  `('should create array with $input entries',
    ({ input, result }) => expect(repeat(input)).toEqual(result));


});